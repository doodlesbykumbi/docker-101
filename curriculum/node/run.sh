#!/usr/bin/env bash

# one time sh
#docker run --rm -it -v node_modules:/node_modules example-node:latest sh

# start with node
docker run --rm --name=example-node -it example-node:latest node index.js
