# example-react

## Installation
Clone this project and install the dependencies with `yarn`:
```
yarn install
```

## Development
Run the command below to start a development server at `http://localhost:3000`:
```
yarn start
```
