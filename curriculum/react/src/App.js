import React, { Component } from 'react';

import './App.scss';

class App extends Component {

  render() {
    return (
      <div>Hello from React </div>
    );
  }
}

export default App;
