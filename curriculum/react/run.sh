#!/usr/bin/env bash

# dev
docker run \
  --rm \
  -v node_modules:/node_modules \
  -v $PWD/src:/usr/src/app/src \
  --name example-react-dev \
  -p 3000:3000 example-react-dev:latest

# local prod
docker run \
  --rm \
  --name example-react \
  -p 8080:80 example-react:latest

# prod
docker pull registry.gitlab.com/doodlesbykumbi/docker-101-exmple
docker run \
  --rm \
  --name example-react \
  -p 8080:80 registry.gitlab.com/doodlesbykumbi/docker-101-exmple
