#!/usr/bin/env bash

docker build -t example-react:latest .
docker build -t example-react-dev:latest -f Dockerfile.dev .
