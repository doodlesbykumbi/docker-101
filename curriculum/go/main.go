package main

import (
  "net/http"
  "os"
  "strings"
  "fmt"
)

func sayHello(w http.ResponseWriter, r *http.Request) {
  message := r.URL.Path
  message = strings.TrimPrefix(message, "/")
  message = "Hello " + message
  w.Write([]byte(message))
}

func main() {
  fs := http.FileServer(http.Dir("."))
  http.Handle("/fs/", http.StripPrefix("/fs", fs))
  http.HandleFunc("/", sayHello)

  port := "8080"
  if portEnv, ok := os.LookupEnv("PORT"); ok {
    portEnv = portEnv
  }

  fmt.Printf("Listening on :%v\n", port)
  if err := http.ListenAndServe(":" + port, nil); err != nil {
    panic(err)
  }
}
