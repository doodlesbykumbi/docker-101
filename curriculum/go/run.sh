#!/usr/bin/env bash

docker run \
  --rm \
  --name example-go \
  -v "$PWD/../..":/extern \
  -p 8080:8080 example-go:latest
